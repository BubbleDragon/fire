/**
 * @file thread.cpp
 * @author Alexander Smirnov
 *
 * This file is a part of the FIRE package.
 * This file is used to compile FLAME binaries.
 *
*/

#include "functions.h"
#include "parser.h"

string common::FIRE_folder;
string common::config_file;

#ifdef WITH_DEBUG

#include <execinfo.h>
#include <csignal>

void handler(int sig) {
    void *array[32];
    char **messages = nullptr;
    int size;

    // get void*'s for all entries on the stack
    size = backtrace(array, 32);

    // print out all the frames to stderr
    fprintf(stderr, "Error: signal %d:\n", sig);

    cerr << "Quick backtrace summary" << endl;
    backtrace_symbols_fd(array, size, STDERR_FILENO);

    cout << "Detailed backtrace summary" << endl;
    messages = backtrace_symbols(array, size);
    /* skip first stack frame (points here) */
    printf("[bt] Execution path:\n");
    for (int i = 1; i < size; ++i) {
        printf("[bt] #%d %s\n", i, messages[i]);
        char syscom[256];
#ifdef PRIME
        char c = 'p';
#else
        char c = ' ';
#endif
        sprintf(syscom, "addr2line %p -f -p -i -e %sFLAME6%c", array[i], common::FIRE_folder.c_str(), c);
        if (system(syscom)) {
            printf("Install addr2line for more details next time\n");
        }
    }
    exit(2);
}
#endif

/**
* FLAME binaries can be started either the same way like FIRE for the port connection (see first test)
* or called by FIRE.
* FLAME binaries can be started manually to work in a particular sector with -sector sector_number,
* but only with already existing database.
* Positive numbers mean forward pass, negative - backward.
* @param argc number of arguments
* @param argv array of arguments
* @return successfullness
*/
int main(int argc, char *argv[]) {

    timeval start_time{};
    gettimeofday(&start_time, nullptr);


#ifdef WITH_DEBUG
    signal(SIGSEGV, handler);  //install handler to print errors
    signal(SIGBUS, handler);   //bus error, significant memory problems
    signal(SIGABRT, handler);  // comes from abort()
#endif
    common::remote_worker = true;
    int sector;
    int thread_number;
    string output;
    set<point, indirect_more> points;

    if ((argc == 2) && !strcmp(argv[1], "-test")) {
        printf("Ok\n");
        return 0;
    }

    char current[PATH_MAX];
    if (!getcwd(current, PATH_MAX)) {
        cerr << "Can't get current dir name" << endl;
        return 1;
    }
    string scurrent = (string) current;
    string srun = (string) argv[0];

    // this is important, there's the length of program name here!
#ifdef PRIME
    srun = srun.substr(0, srun.length() - 7);
#else
    srun = srun.substr(0, srun.length() - 6);
#endif

    getUserName(common::username);
    if (srun[0] == '/') {  // running with full path
        common::FIRE_folder = srun;
    } else { //relative path, using current dir
        common::FIRE_folder = scurrent + "/" + srun;
    }

    pair<int,int> temp = parseArgcArgv(argc, argv, false);
    thread_number = temp.first;
    sector = temp.second;

    bool with_fire = true;
    if (thread_number == -1) {
        cout << "Forcing no send to parent" << endl;
        with_fire = false;
        thread_number = 0;
    }

    if (parse_config((common::config_file + ".config").c_str(), points, output, sector, !with_fire)) {
        return -1;
    }

    if (sector > 0) { // forward or just the one pass in case of reverse mode!
        forward_stage(thread_number, sector);
    } else if (sector < 0) {// backward
        perform_substitution(thread_number, -sector);
    } else { // receive tasks from master
        if (!common::port) {
            cout << "Port not set, won't be able to connect to FIRE"<<endl;
            return 1;
        }
        if (common::cpath == "") {
            cout << "Storage folder not set, won't be able to exchange databases with FIRE"<<endl;
            return 1;
        }
        if (common::cpath_on_substitutions && (common::threads_number != common::sthreads_number)) {
            cout << "The number of threads and substitution threads should be equal in case of FLAME and storage on substitutions"<<endl;
            return 1;
        }
        work_with_master();
        cout << "Master communication closed" << endl;
    }

    if (common::send_to_parent) {
        fclose(common::child_stream_from_child);
        fclose(common::child_stream_to_child);
    }

    if ((!common::send_to_parent) || (common::receive_from_child)) {
        closeCalc();
        equation::f_stop = true;

        for (unsigned int i = 0; i != common::fthreads_number; ++i) {
            sem_post(equation::f_submit_sem[i % common::f_queues]);
        }
        for (unsigned int i = 0; i != common::fthreads_number; ++i) {
            pthread_join(equation::f_threads[i], nullptr);
        }

        // clean up semaphores
        char sname[32];
        char rname[32];
        int pid = (int) getpid();
        for (unsigned int i = 0; i != common::f_queues; ++i) {
            sprintf(sname, "%s:FIRE_fs%d:%d", common::username, i, pid);
            sem_unlink(sname);
        }
        for (unsigned int i = 0; i != common::lthreads_number; ++i) {
            sprintf(rname, "%s:FIRE_fr%d:%d", common::username, i, pid);
            sem_unlink(rname);
        }
    }
    free(common::sector_numbers_fast);
    for (unsigned int i = 0; i != (1u << (common::dimension)); ++i) {
        if (common::orderings_fast != nullptr) {
            free(common::orderings_fast[i]);
        }
    }
    free(common::orderings_fast);
    if (!sector) cout<<"Done"<<endl;


    timeval stop_time{};
    gettimeofday(&stop_time, nullptr);
    timeval diff_time{};
    timeval_subtract(&diff_time, &stop_time, &start_time);
    if (!common::silent) {
        cout << "FLAME time ("<<sector<<"): " << (double) diff_time.tv_sec + ((double) diff_time.tv_usec / 1000000.0) << endl;
    }

    return 0;
}
