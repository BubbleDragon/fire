/**
 * @file main.cpp
 * @author Alexander Smirnov
 *
 * This file is a part of the FIRE package.
 */

#include "common.h"
#include "parser.h"
#include "functions.h"
#include <cstdio>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <cstring>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>


// routines to save and load temporary tables
/**
 * Temporary tables.
 */
map<point, vector<pair<point, COEFF> > > temp;

/**
 * Set terms for specific point in temporary tables.
 * @param p point for which we are adding terms to temporary tables.
 * @param terms terms which we are writing in temporary tables.
 */
void p_set_temp(const point &p, const vector<pair<point, COEFF> > &terms) {
    temp.emplace(p, terms);
}

/**
 * Get terms for point from temporary tables.
 * @param p point for which we are searching terms.
 * @param terms result vector where we'll place found terms, if there're any.
 */
void p_get_temp(const point &p, vector<pair<point, COEFF> > &terms) {
    auto itr = temp.find(p);
    if (itr == temp.end()) {
        terms.clear();
    } else {
        terms = itr->second;
    }
}

string common::FIRE_folder;
string common::config_file;

#ifdef WITH_DEBUG

#include <execinfo.h>
#include <csignal>

void handler(int sig) {
    void *array[32];
    char **messages = nullptr;
    int size;

    // get void*'s for all entries on the stack
    size = backtrace(array, 32);

    // print out all the frames to stderr
    fprintf(stderr, "Error: signal %d:\n", sig);

    cerr << "Quick backtrace summary" << endl;
    backtrace_symbols_fd(array, size, STDERR_FILENO);

    cout << "Detailed backtrace summary" << endl;
    messages = backtrace_symbols(array, size);
    /* skip first stack frame (points here) */
    printf("[bt] Execution path:\n");
    for (int i = 1; i < size; ++i) {
        printf("[bt] #%d %s\n", i, messages[i]);
        char syscom[256];
#ifdef PRIME
        char c = 'p';
#else
        char c = ' ';
#endif
        sprintf(syscom, "addr2line %p -f -p -i -e %sFIRE6%c", array[i], common::FIRE_folder.c_str(), c);
        if (system(syscom)) printf("Install addr2line for more details next time");
    }
    exit(2);
}
#endif

/**
 * Launches reduction. For the list of options see the paper.
 * @param argc number of arguments
 * @param argv array of arguments
 * @return successfullness
 */
int main(int argc, char *argv[]) {
#ifdef WITH_DEBUG
    signal(SIGSEGV, handler);  //install handler to print errors
    signal(SIGBUS, handler);  //bus error, significant memory problems
#endif
    cout << "FIRE 6.2" << endl;
    if ((argc == 2) && !strcmp(argv[1], "-test")) {
        printf("Ok\n");
        return 0;
    }
    char current[PATH_MAX];
    if (!getcwd(current, PATH_MAX)) {
        cout << "Can't get current dir name" << endl;
        return 1;
    }
    string scurrent = (string) current;
    string srun = (string) argv[0];
#ifdef PRIME
    srun = srun.substr(0, srun.length() - 6);
#else
    srun = srun.substr(0, srun.length() - 5);
#endif
    getUserName(common::username);
    if (srun[0] == '/') {  // running with full path
        common::FIRE_folder = srun;
    } else { //relative path, using current dir
        common::FIRE_folder = scurrent + "/" + srun;
    }

#ifndef SMALL_POINT
    if (sizeof(point) != 24) {
        cout << "Strange size of point: " << sizeof(point) << endl;
        throw 1;
    }
#else
    if (sizeof(point) != 16) {
        cout << "Strange size of point: " << sizeof(point) << endl;
        throw 1;
    }
#endif

    parseArgcArgv(argc, argv, true);

    set<point, indirect_more> points;

    string output;
    if (parse_config((common::config_file + ".config").c_str(), points, output, 0) != 0) {
        return -1;
    }

    if (common::port) {

        struct ifaddrs *ifAddrStruct = nullptr;
        struct ifaddrs *ifa = nullptr;
        void *tmpAddrPtr = nullptr;

        getifaddrs(&ifAddrStruct);
        string address;
        for (ifa = ifAddrStruct; ifa != nullptr; ifa = ifa->ifa_next) {
            if (!ifa->ifa_addr) {
                continue;
            }
            if (ifa->ifa_addr->sa_family == AF_INET) { // check it is IP4
                // is a valid IP4 Address
                tmpAddrPtr = &((struct sockaddr_in *) ifa->ifa_addr)->sin_addr;
                char addressBuffer[INET_ADDRSTRLEN];
                inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
                if (strcmp(addressBuffer, "127.0.0.1") != 0) {
                    address = addressBuffer;
                }
            } // we don't work with IP6
        }
        if (ifAddrStruct != nullptr) freeifaddrs(ifAddrStruct);
        if (!common::silent) {
            if (address != "") {
                cout << "Current IP: " << address << endl;
            }
        }

        if (!common::parallel_mode) {
            ofstream write;
            if (common::cpath != "") {
                write.open((common::cpath + "IP").c_str(), ios::out | ios::binary);
            } else {
                write.open((common::path + "IP").c_str(), ios::out | ios::binary);
            }
            write << address << endl;
            write.close();
        }

    }


    map<unsigned short, set<point> > needed;
    for (const auto &pnt : points) {
        add_needed(needed, pnt);
    }

    for (const auto &item : needed) {
        if (common::wrap_databases) {
            database_to_file_or_back(item.first, false);
        }
        open_database(common::points + item.first, common::msiz, item.first);

        vector<pair<point, COEFF> > t;
        for (const auto &pnt : item.second) {
            p_set(pnt, t, 254);
        }

        close_database(common::points + item.first, item.first);
        if (common::wrap_databases) {
            database_to_file_or_back(item.first, true);
        }
    }

    // the main call to reduction in functions.cpp
    // the data to be evaluated is already in tables
    if (!points.empty()) {
        Evaluate();
    }

    if (common::receive_from_child) {
        closeCalc();
        equation::f_stop = true;

        for (unsigned int i = 0; i != common::fthreads_number; ++i) {
            sem_post(equation::f_submit_sem[i % common::f_queues]);
        }
        for (unsigned int i = 0; i != common::fthreads_number; ++i) {
            pthread_join(equation::f_threads[i], nullptr);
        }

        // clean up semaphores
        char sname[32];
        char rname[32];
        int pid = (int) getpid();
        for (unsigned int i = 0; i != common::f_queues; ++i) {
            sprintf(sname, "%s:FIRE_fs%d:%d", common::username, i, pid);
            sem_unlink(sname);
        }
        for (unsigned int i = 0; i != common::threads_number; ++i) {
            sprintf(rname, "%s:FIRE_fr%d:%d", common::username, i, pid);
            sem_unlink(rname);
        }
    }

    set<point, indirect_more> masters;

    if (!common::only_masters) {
        //reading expressions from all databases, filling temporary tables, creating list of masters;
        vector<pair<point, COEFF> > empty_terms;
        for (const auto &item : needed) {
            if (common::wrap_databases) {
                database_to_file_or_back(item.first, false);
            }
            open_database(common::points + item.first, common::msiz, item.first);

            for (const auto &pnt : item.second) {
                vector<pair<point, COEFF> > terms;
                p_get(pnt, terms);
                if (terms.empty()) {
                    masters.insert(pnt);
                } else {
                    p_set_temp(pnt, terms);
                    for (const auto &term : terms) {
                        if (!(term.first == pnt)) {
                            masters.insert(term.first);
                            p_set_temp(term.first, empty_terms);
                        }
                    }
                }
            }
            close_database(common::points + item.first, item.first);
            if (common::wrap_databases) {
                database_to_file_or_back(item.first, true);
            }
        }
    } else { // now the only masters way
        // we need to locate masters and add them to the final list
        cout << "Identifying master-integrals" << endl;
        for (unsigned short test_sector = 2; test_sector <= common::abs_max_sector; ++test_sector) {
            if (!database_exists(test_sector)) continue;
            if (common::wrap_databases) {
                database_to_file_or_back(test_sector, false);
            }
            open_database(common::points + test_sector, common::msiz, test_sector);

            auto *pdb = (kyotocabinet::PolyDB *) common::points[test_sector];
            class VisitorImpl : public kyotocabinet::DB::Visitor {
                // call back function for an existing record
                const char *visit_full(const char *kbuf, size_t ksiz, const char *vbuf, size_t vsiz, size_t *sp) override {
                    if (ksiz < sizeof(point)) {
                        return NOP;
                    }
                    const point test = *(reinterpret_cast<const point *>(kbuf));
                    if (((unsigned char *) vbuf)[2] == 255) { // marked as master
                        masters->insert(test);
                    }
                    return NOP;
                }

                // call back function for an empty record space
                const char *visit_empty(const char *kbuf, size_t ksiz, size_t *sp) override {
                    return NOP;
                }
            public:
                set<point, indirect_more> *masters{};
            } visitor;

            visitor.masters = &masters;

            if (!pdb->iterate(&visitor, false)) {
                cout << "Iterate error on master collection: " << test_sector << endl;
                throw 1;
            }

            close_database(common::points + test_sector, test_sector);
            if (common::wrap_databases) {
                database_to_file_or_back(test_sector, true);
            }
        }
    }
    cout << "Master integrals: " << masters.size() << endl;
    points.clear();
    points = masters;

    //replacing vectors in points with their original vectors in orbits
    if (!common::only_masters) {
        for (auto &integral : equation::initial) {
            point p = integral.second;
            if (!p.is_zero()) {  // the original vector was not mapped into zero
                if (integral.first != p.get_vector()) { // the vectors differ. we do not have such a point
                    point new_p(integral.first);
                    vector<pair<point, COEFF> > terms;
                    p_get_temp(p, terms);
                    if (!terms.empty()) {   // there is some table entry for our point, we are making a copy
                        COEFF c = terms.back().second;
                        terms.pop_back();
                        terms.emplace_back(new_p, c);
                    } else {    // there is no entry for our point, it is a master, sending the new one to the old one
                        COEFF one;
                        COEFF minus_one;
#ifdef PRIME
                        one.n = 1;
                        minus_one.n = common::prime - 1;
#else
                        one.s = "1";
                        minus_one.s = "-1";
#endif
                        terms.emplace_back(p, minus_one);
                        terms.emplace_back(new_p, one);
                    }
                    p_set_temp(new_p, terms);
                    points.insert(new_p);
                } else { //such a point already exists
                    points.insert(p);
                }
            } else {  // it is a zero point, mapping it to zero
                vector<t_index> vv = integral.first;

                point new_p(vv,0,-2); // get it to sector 1 without changes
                COEFF c;
#ifdef PRIME
                c.n = 1;
#else
                c.s = "1";
#endif
                vector<pair<point, COEFF> > terms;
                terms.emplace_back(new_p, c);

                p_set_temp(new_p, terms);
                points.insert(new_p);
            }
        }
    }

    // everything done, saving tables
    cout << "Saving tables" << endl;

    fstream out;
    out.open(output.c_str(), fstream::out);

    out << "{" << endl << "    {" << endl;
    for (auto itr = points.begin(); itr != points.end(); ++itr) {
        out << "        {" << itr->number() << "," << endl;
        out << "            {" << endl;
        vector<pair<point, COEFF> > terms;
        p_get_temp(*itr, terms);
        if (terms.empty()) {
            out << "{" << itr->number() << ",\"1\"}}}";
        } else {
            for (unsigned int i = 0; i != terms.size() - 1; ++i) {
                out << "                {" << terms[i].first.number() << "," << "\"";
#ifdef PRIME
                uint128_t num, denum;
                num = terms[i].second.n;
                denum = terms.back().second.n;
                num = common::prime - num;
                denum = mul_inv(denum, common::prime);
                num *= denum;
                num %= common::prime;
                out << (uint64_t) num;
#else
                out << "-(" << terms[i].second.s << ")/(" << terms.back().second.s << ")";
#endif
                out << "\"" << "}";
                if (i + 2 != terms.size()) {
                    out << ",";
                }
                out << endl;

            }
            out << "            }" << endl;
            out << "        }";
        }
        itr++;
        if (itr != points.end()) {
            out << "," << endl;
        }
        itr--;
    }
    out << endl << "    }," << endl;

    out << "    {" << endl;
    for (auto itr = points.begin(); itr != points.end(); ++itr) {
        out << "        {" << itr->number() << "," << *itr << "}";
        itr++;
        if (itr != points.end()) {
            out << ",";
        }
        itr--;
        out << endl;
    }
    out << "    }" << endl << "}" << endl;
    out.close();

    if (common::wrap_databases) {
        database_to_file_or_back(0, true); // finish the wrapper thread
    }

    free(common::sector_numbers_fast);
    for (unsigned int i = 0; i != (1u << (common::dimension)); ++i) {
        if (common::orderings_fast != nullptr) {
            free(common::orderings_fast[i]);
        }
    }
    free(common::orderings_fast);
    if (common::parallel_mode) {
        char sys_command[100];
        sprintf(sys_command, "rm -r %s", common::path.c_str());
        if (system(sys_command)) {
            printf("Could not clean up database after MPI");
        }
    }

    if (common::port != 0) {
        // we connect to ourself to make the socket_listen_thread stop
        int sockfd;
        struct sockaddr_in serv_addr{};
        struct hostent *server;

        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0) {
            cout << "ERROR opening socket" << endl;
            throw 1;
        }

        server = gethostbyname("localhost");
        if (server == nullptr) {
            fprintf(stderr, "ERROR, no such host\n");
            throw 1;
        }
        bzero((char *) &serv_addr, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        bcopy(server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
        serv_addr.sin_port = htons(common::port);
        if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
            cout << "Error connecting" << endl;
            throw 1;
        }
        char buffer[] = "Stop";
        ssize_t n = write(sockfd, buffer, 5);
        if (n!=5) {
            cout << "CANNOT STOP CHILD"<<endl;
        }
        pthread_join(common::socket_listen, nullptr);
    }


    return 0;
}
