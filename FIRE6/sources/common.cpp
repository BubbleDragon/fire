/** @file common.cpp
 *  @author Alexander Smirnov
*
*  This file is a part of the FIRE package.
*  It contains the initializations of static variables in the common class
*  and a number of general functions used in other parts of the program.
*/


#include "common.h"

/* initializations of static variables
* refer to common.h for details
*/

pthread_t common::socket_listen = 0;

unsigned short common::dimension;
unsigned short *common::sector_numbers_fast;
t_index **common::orderings_fast;

int common::pos_pref = 1;

bool common::silent = false;

bool common::ftool = false;

bool common::hint;
string common::hint_path;

t_compressor common::compressor = C_LZ4;

uint64_t common::prime = 0; // the prime number for fast evaluations
unsigned short common::prime_number = (unsigned short)-1; //by default no prime is selected


map<string, string> common::variable_replacements; // while parsing

bool common::nolock = false;
bool common::all_ibps = false;

int common::print_step = 0;

bool common::split_masters = false;
unsigned int common::master_number_min = 0;
unsigned int common::master_number_max = 0;

bool common::variables_set_from_command_line = false;

char common::username[128];

int common::virtual_sector = 0;

bool common::parallel_mode = false;

string common::path = "";
string common::cpath;

bool common::cpath_on_substitutions = false;
int common::buckets[MAX_SECTORS + 1];
int64_t common::buckets_full[MAX_SECTORS + 1];

string common::tables_prefix = "";
vector<unsigned short> common::var_values_from_arv = {};


bool common::only_masters = false;
bool common::keep_all = false;

// pipes for expression communication
FILE *common::child_stream_from_child = nullptr;
FILE *common::child_stream_to_child = nullptr;
bool common::receive_from_child = false;
bool common::send_to_parent = false;

unsigned short common::port = 0;

bool common::remote_worker = false;
bool common::small = false;

int common::abs_max_sector = 3;

bool common::memory_db = false;

int common::abs_max_level = 0;

int common::abs_min_level = 100;
KCDB *common::points[MAX_SECTORS + 1];

uint64_t common::c_time;
uint64_t common::thread_time;

unsigned long common::eqs_total = 0;
unsigned long common::eqs_max = 0;

int common::msiz = 26;

unsigned short common::global_pn = 0;
unsigned int common::threads_number = 0;
unsigned int common::lthreads_number = 1;
unsigned int common::sthreads_number = 0;
unsigned int common::fthreads_number = 0;
unsigned int common::f_queues = 1;

vector<pair<string, vector<vector<t_index> > > >  common::ibps;

map<unsigned short, vector<vector<t_index> > > common::iorderings;

vector<vector<vector<t_index> > > common::symmetries;

vector<vector<t_index> > common::ssectors;
set<SECTOR> common::lsectors;

map<unsigned short, vector<pair<vector<pair<vector<t_index>, pair<short, bool> > >, vector<pair<string, vector<pair<vector<t_index>, short> > > > > > > common::lbases;
//      sector    condition+result        coefficients  free term   eq                 coeff     indices:   coefficients and free term

// database wrapper things
pthread_mutex_t common::wrapper_mutex;
pthread_mutex_t common::wrapper_submitter_mutex;
pthread_t common::wrapper_thread;
pthread_cond_t common::wrapper_cond;
volatile int common::database_request;
bool common::wrap_databases;
bool common::wrapper_result;


vector<t_index> sector(const vector<t_index> &v) {
    vector<t_index> result;
    for (const t_index i : v)
        if (i > 0) {
            result.push_back(1);
        } else {
            result.push_back(-1);
        }
    return result;
}

SECTOR sector_fast(const vector<t_index> &v) {
    SECTOR result = 0;
    for (const t_index i : v)
        if (i > 0) {
            result = ((result << 1) ^ 1);
        } else {
            result = result << 1;
        }
    return result;
}

vector<t_index> corner(const vector<t_index> &v) {
    vector<t_index> result;
    for (const t_index i : v)
        if (i > 0) {
            result.push_back(1);
        } else {
            result.push_back(0);
        }
    return result;
}


vector<t_index> degree(const vector<t_index> &v) {
    vector<t_index> result;
    for (const t_index i : v)
        if (i > 0) {
            result.push_back(i - 1);
        } else {
            result.push_back(-i);
        }
    return result;
}


pair<unsigned int, unsigned int> level(const vector<t_index> &v) {
    unsigned int p = 0;
    unsigned int m = 0;
    for (const t_index i : v)
        if (i > 0) {
            p += (i - 1);
        } else {
            m += (-i);
        }
    return make_pair(p,m);
}


// output routines for vectors and double vectors
void print_vector(const vector<t_index> &v) {
    if (common::silent) return;
    auto it = v.begin();
    cout << "{" << (int) (*it);
    for (it++; it != v.end(); it++) {
        cout << "," << (int) (*it);
    }
    cout << "}";
}

// output routines for vectors and double vectors
void print_sector_fast(const SECTOR &sf) {
    if (common::silent) return;
    SECTOR i = 1;
    i <<= (common::dimension -1);
    cout << "{" << (i&sf ? 1 : -1);
    for (i>>=1; i; i>>=1) {
        cout << "," << (i&sf ? 1 : -1);
    }
    cout << "}";
}


void Orbit(const vector<t_index> &v, vector<vector<t_index> > &orbit, vector<vector<vector<t_index> > > &sym) {
    for (const auto &values : sym) {
        if (values.size() == 4) {
            const vector<t_index> &restrictions = values[2];
            for (unsigned int i = 0; i != v.size(); ++i) {
                if ((restrictions[i] == 1) && (v[i] <= 0)) continue;
                if ((restrictions[i] == -1) && (v[i] > 0)) continue;
            }
        }
        const vector<t_index> &permutation = values[0];
        vector<t_index> result;
        for (unsigned int i = 0; i != v.size(); ++i) {
            result.push_back(v[permutation[i] - 1]);
        }
        bool to_add = true;
        for (unsigned int i = (*(values.rbegin()))[0]; i != result.size(); ++i) {
            if (result[i] > 0) {
                to_add = false;
            }
        }
        if (to_add) orbit.push_back(result);
    }
}

void make_ordering(t_index *mat, const vector<t_index> &sector) {
    vector<vector<t_index> > result;
    result.reserve(sector.size());
    vector<t_index> neg;
    vector<t_index> one_vector;
    vector<t_index> zero_vector;
    zero_vector.reserve(sector.size());

    bool all_plus = true;
    bool all_minus = true;
    for (const t_index i : sector) {
        if (i != 1) all_plus = false;
        if (i != -1) all_minus = false;
    }

    auto l_pos = static_cast<unsigned int>(-1);
    auto l_neg = static_cast<unsigned int>(-1);

    for (unsigned int i = 0; i < sector.size(); ++i) {
        one_vector.push_back(1);
        zero_vector.push_back(0);
        if (sector[i] == 1) {
            l_pos = i;
            neg.push_back(0);
        } else {
            l_neg = i;
            neg.push_back(1);
        }
    }

    if ((all_plus) || all_minus) {
        result.push_back(one_vector);
        for (unsigned int i = 0; i + 1 < sector.size(); ++i) {
            vector<t_index> v;
            for (unsigned int j = 0; j < sector.size(); ++j) {
                if (i == j) {
                    v.push_back(1);
                } else {
                    v.push_back(0);
                }
            }
            result.push_back(v);
        }
    } else {
        if (l_pos == static_cast<unsigned int>(-1) || l_neg == static_cast<unsigned int>(-1)) {
            cout << "Something wrong with make_ordering!" << endl;
            throw 1;
        }
        result.push_back(one_vector);
        result.push_back(neg);
        for (unsigned int i = 0; i < sector.size() - 1; ++i) {
            if (sector[i] == 1) {
                if (i != l_pos) {
                    vector<t_index> v = zero_vector;
                    v[i] = 1;
                    result.push_back(v);
                }
            } else {
                if (i != l_neg) {
                    neg[i] = 0;
                    result.push_back(neg);
                }
            }
        }
    }

    int row = 0;
    for (auto itr_row = result.begin(); itr_row != result.end(); ++itr_row, ++row) {
        // now we stopped using vectors, moving to arrays, it's much faster
        // and array of vectors leads to crashes
        int column = 0;
        for (auto itr_column = itr_row->begin(); itr_column != itr_row->end(); ++itr_column, ++column) {
            mat[row * common::dimension + column] = *itr_column;
        }
    }
}


vector<vector<t_index> > all_sectors(unsigned int d, int positive, int positive_start) {
    vector<t_index> vv3;
    for (unsigned int i = 0; i != d; ++i) vv3.push_back(-1);
    vector<vector<t_index> > vv;
    vv.push_back(vv3);
    for (int i = positive_start - 1; i < positive; ++i) {
        vector<vector<t_index> > ww = vv;
        vv.reserve(2 * vv.size());
        for (auto &current : ww) {
            current[i] = 1;
            vv.push_back(current);
        };
    };
    return (vv);
};


void clear_database(int number) {
    if (number != 0) {
        remove((common::path + int2string(number) + ".kch").c_str());
    } else {
        remove((common::path + int2string(number) + ".kct").c_str());
    }
}

bool database_exists(int number) {
    if (common::wrap_databases) {
        return database_to_file_or_back(number | 65536, false);
    }
    string name = common::path + int2string(number);
    name += common::memory_db ? ".tmp" : ".kch";
    bool result = (access(name.c_str(), F_OK) != -1);
    return result;
}

void open_database(KCDB **db, int msiz, int number, bool reorganize) {
    common::buckets_full[number] = (1ll << common::buckets[number]);
    if (common::memory_db) {
        common::buckets_full[number] *= 2;  //we need less buckets for memory_db
    }

    *db = kcdbnew();
    auto *pdb = reinterpret_cast<kyotocabinet::PolyDB *> (*db);

    uint32_t flags = KCOWRITER | KCOCREATE;
    if (reorganize) flags |= KCOREORGANIZE;
    if (common::nolock) flags |= KCONOLOCK;

    if (number == 0) {
        cout << "We should not be opening database 0 here!" << endl;
        throw 1;
    }

    // no preprocessor instructions for zlib since it will be compiled properly anyway
    // we do not allow the options at parser level
    if (common::memory_db) {
        auto *ldb = new kyotocabinet::CacheDB();
        ldb->tune_buckets((long long int) pow(2.0, common::buckets[number]));
        if (common::compressor != C_NONE) {
            ldb->tune_options(kyotocabinet::HashDB::TLINEAR | kyotocabinet::HashDB::TCOMPRESS);
        }
        switch (common::compressor) {
            case C_SNAPPY:
#ifdef WITH_SNAPPY
                ldb->tune_compressor(new SnappyCompressor());
#endif
                break;
            case C_ZLIB:
                ldb->tune_compressor(new kyotocabinet::ZLIBCompressor<kyotocabinet::ZLIB::RAW>);
                break;
            case C_LZ4:
                ldb->tune_compressor(new LZ4Compressor);
                break;
            case C_LZ4FAST:
                ldb->tune_compressor(new LZ4FastCompressor);
                break;
            case C_LZ4HC:
                ldb->tune_compressor(new LZ4HCCompressor);
                break;
            case C_ZSTD:
#ifdef WITH_ZSTD
                ldb->tune_compressor(new ZstdCompressor);
#endif
                break;
            case C_NONE:;
        }

        if (!ldb->open("*", KCOWRITER | KCOCREATE)) {
            cout << "Error opening database, exiting" << endl;
            delete(ldb);
            throw 1;
        }
        pdb->type_ = kyotocabinet::BasicDB::TYPECACHE;
        pdb->db_ = (kyotocabinet::BasicDB *) ldb;
        kcdbloadsnap(*db, (common::path + int2string(number) + ".tmp").c_str());
    } else {
        auto *ldb = new kyotocabinet::HashDB();
        ldb->tune_buckets((long long int) pow(2.0, common::buckets[number]));
        ldb->tune_defrag(1);
        ldb->tune_fbp(10);
        ldb->tune_alignment(8);
        ldb->tune_map((long long int) pow(2.0, msiz));
        if (common::compressor != C_NONE) {
            ldb->tune_options(kyotocabinet::HashDB::TLINEAR | kyotocabinet::HashDB::TCOMPRESS);
        }
        switch (common::compressor) {
            case C_SNAPPY:
#ifdef WITH_SNAPPY
                ldb->tune_compressor(new SnappyCompressor());
#endif
                break;
            case C_ZLIB:
                ldb->tune_compressor(new kyotocabinet::ZLIBCompressor<kyotocabinet::ZLIB::RAW>);
                break;
            case C_LZ4:
                ldb->tune_compressor(new LZ4Compressor);
                break;
            case C_LZ4FAST:
                ldb->tune_compressor(new LZ4FastCompressor);
                break;
            case C_LZ4HC:
                ldb->tune_compressor(new LZ4HCCompressor);
                break;
                case C_ZSTD:
#ifdef WITH_ZSTD
                ldb->tune_compressor(new ZstdCompressor);
#endif
                break;
            case C_NONE:;
        }
        if (!ldb->open(common::path + int2string(number) + ".kch", flags)) {
            cout << "Error opening database (" << ldb->error().message() << "), exiting" << endl;
            delete(ldb);
            throw 1;
        }
        pdb->type_ = kyotocabinet::BasicDB::TYPEHASH;
        pdb->db_ = (kyotocabinet::BasicDB *) ldb;
    }
    int64_t entries = kcdbcount(*db);
    if (2 * entries > common::buckets_full[number]) {
        while (2 * entries > common::buckets_full[number]) {
            common::buckets[number]++;
            common::buckets_full[number] *= 2;
        }
        reopen_database(db, msiz, number);
    }
}


void file_not_needed(string path) {
    int fd;
    fd = open(path.c_str(), O_RDONLY);
    if (fd == -1) {
        cout << "File open error: "<< path;
        throw 1;
    }
#ifdef F_FULLFSYNC
    // OS X
    fcntl(fd, F_FULLFSYNC);
#else
    fdatasync(fd);
#endif

#ifndef POSIX_FADV_DONTNEED
    fcntl(fd, F_NOCACHE, 1);
#else
    // the preferred old way
    posix_fadvise64(fd, 0, 0, POSIX_FADV_DONTNEED);
#endif
    close(fd);
}


void reopen_database(KCDB **db, int msiz, int number) {
    if (!common::silent) {
        cout << "Reopening database " << number << " with bucket=" << common::buckets[number] << endl;
    }
    string path = common::path + int2string(number) + ".tmp";
    if (!common::memory_db) kcdbdumpsnap(*db, path.c_str());
    close_database(db, number);
    if (!common::memory_db) clear_database(number);
    open_database(db, msiz, number);
    if (!common::memory_db) kcdbloadsnap(*db, path.c_str());
    file_not_needed(path);
}

void close_database(KCDB **db, int number) {
    if (common::memory_db) {
        kcdbdumpsnap(*db, (common::path + int2string(number) + ".tmp").c_str());
        file_not_needed(common::path + int2string(number) + ".tmp");
    }
    kcdbclose(*db);
    kcdbdel(*db);

    string path = common::path + int2string(number);
    if (number == 0) {
        path = path + ".kct";
    } else {
        path = path + ".kch";
    }
    if (!common::memory_db) file_not_needed(path);
}


bool in_lsectors(unsigned short test_sector) {
    if (common::lsectors.empty()) return true;
    return (common::lsectors.find(sector_fast(common::ssectors[test_sector]))!= common::lsectors.end());
}


int positive_index(vector<t_index> v) {
    int res = 0;
    for (unsigned int i = 0; i != v.size(); ++i) {
        if (v[i] == 1) res++;
    }
    return res;
}

string int2string(int i) {
    stringstream ss(stringstream::out);
    if (i < 1000) ss << 0;
    if (i < 100) ss << 0;
    if (i < 10) ss << 0;
    ss << i;
    return ss.str();
}


void read_from_stream(char **buf, int *buf_size, FILE *stream_from_child) {
    char *pos = *buf;
    int read = 0;
    int rem_size = *buf_size;
    while (true) {
        pos[rem_size - 2] = '\r'; // just to check whether it will be overwritten
        char * temp = fgets(pos, rem_size, stream_from_child);
        if (feof(stream_from_child)) return;
        if (!temp || ferror(stream_from_child)) {
            cout << "Error reading from stream" << endl;
            throw 1;
        }
        if (pos[rem_size - 2] == '\r')
            return; // we did not overwrite the prelast symbol, so we are done with reading the new line

        // if we are here, this means that the prelast symbol was overwritten
        if (pos[rem_size - 2] == '\n') return; // '\n' is the last symbol of what we've been sending
        if (pos[rem_size - 2] == '\0') return; // prelast overwritten with end
        *buf_size = (*buf_size) * 2;
        *buf = (char *) realloc((void *) *buf, static_cast<size_t>(*buf_size));
        read += (rem_size - 1);
        rem_size = (*buf_size) - read;
        pos = (*buf) + read;
    }
}


void copy_database(unsigned short number, bool from_storage) {
    string name = int2string(number) + "." + (common::memory_db ? "tmp" : "kch");
    cout << "Copying file " << name << " " << (from_storage ? "from" : "to") << " storage" << endl;
    ifstream src(((from_storage ? common::cpath : common::path) + name).c_str(), ios::binary);
    ofstream dst(((from_storage ? common::path : common::cpath) + name).c_str(), ios::binary);
    dst << src.rdbuf();
    src.close();
    dst.close();
    file_not_needed(common::path + name);
    file_not_needed(common::cpath + name);
}

/**
* a thread worker that wraps and unwraps databases into a global database on request (needed to minimize the number of files on disk)
*/
void *database_wrapper(void *) {
    kyotocabinet::HashDB db;
    db.tune_buckets(4096);
    db.tune_defrag(1);
    if (common::compressor != C_NONE) {
        db.tune_options(kyotocabinet::HashDB::TCOMPRESS);
    }
    switch (common::compressor) {
        case C_SNAPPY:
#ifdef WITH_SNAPPY
            db.tune_compressor(new SnappyCompressor());
#endif
            break;
        case C_ZLIB:
            db.tune_compressor(new kyotocabinet::ZLIBCompressor<kyotocabinet::ZLIB::RAW>);
            break;
        case C_LZ4:
            db.tune_compressor(new LZ4Compressor);
            break;
        case C_LZ4FAST:
            db.tune_compressor(new LZ4FastCompressor);
            break;
        case C_LZ4HC:
            db.tune_compressor(new LZ4HCCompressor);
            break;
        case C_ZSTD:
#ifdef WITH_ZSTD
            db.tune_compressor(new ZstdCompressor);
#endif
            break;
        case C_NONE:;
    }

    // open the wrapper database
    if (!db.open(common::path + "wrapper" + ".kch",
                 kyotocabinet::HashDB::OWRITER | kyotocabinet::HashDB::OCREATE | kyotocabinet::HashDB::OTRUNCATE)) {
        cout << "Wrapper open error: " << db.error().name() << endl;
        throw 1;
    }

    pthread_mutex_lock(&common::wrapper_submitter_mutex); // to send signal
    pthread_cond_signal(&common::wrapper_cond);  // that the database is ready
    pthread_mutex_lock(&common::wrapper_mutex); // we will unlock it as signal wait
    pthread_mutex_unlock(&common::wrapper_submitter_mutex); // main thread can continue with parsing
    while (true) {
        pthread_cond_wait(&common::wrapper_cond,
                          &common::wrapper_mutex);  //  wrapper_mutex is unlocked, sleeping on condition variable
        // received signal, mutex is locked
        if (common::database_request == 0) {
            common::wrapper_result = true;
            break; // request to stop
        }
        if (common::database_request > 0) {
            if (common::database_request & 65536) {
                // check database existence
                string key = int2string(common::database_request & 65535);
                common::wrapper_result = (db.check(key) != -1);
            } else {
                // move snapshot from database to file
                string key = int2string(common::database_request);
                string value;
                if (db.get(key, &value)) {
                    db.remove(key);
                    ofstream dst((common::path + int2string(common::database_request) + "." + "tmp").c_str(), ios::binary);
                    dst << value;
                    dst.close();
                }
                common::wrapper_result = true;
            }
        } else {
            // move snapshot from file to database
            ifstream ifs((common::path + int2string(-common::database_request) + "." + "tmp").c_str(), ios::binary);
            string key = int2string(-common::database_request);
            string value((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));
            ifs.close();
            db.set(key, value);
            remove((common::path + int2string(-common::database_request) + "." + "tmp").c_str());
            common::wrapper_result = true;
        }
        pthread_cond_signal(&common::wrapper_cond); // inform the calling function on result. And it will go on after we go to pthread_cond_wait
    }

    pthread_cond_signal(&common::wrapper_cond);  // inform the calling function on cycle end
    pthread_mutex_unlock(&common::wrapper_mutex);

    db.close();
    pthread_exit(nullptr);
}


// 0 is used to start or stop the wrapper thread
// number directly to move
// number|65536 to check
bool database_to_file_or_back(int number, bool back) {
    if ((number == 0) && !back) {
        pthread_mutex_lock(&common::wrapper_submitter_mutex);
        pthread_create(&common::wrapper_thread, nullptr, database_wrapper, nullptr); // start database_wrapper thread
        pthread_cond_wait(&common::wrapper_cond,
                          &common::wrapper_submitter_mutex);  // waiting for the indication that the database is ready
        pthread_mutex_unlock(&common::wrapper_submitter_mutex);
        return true;
    }

    pthread_mutex_lock(&common::wrapper_submitter_mutex); // so only one thread gets here
    pthread_mutex_lock(&common::wrapper_mutex); // we locked the muter that is related to sleeping condition

    common::database_request = !back ? number : -number; // database to file : file to database

    pthread_cond_signal(&common::wrapper_cond); // we are waking the database_wrapper, but it has to lock the mutex now

    pthread_cond_wait(&common::wrapper_cond,
                      &common::wrapper_mutex);  //  wrapper_mutex is unlocked, sleeping on condition variable waiting for result

    bool result = common::wrapper_result;

    pthread_mutex_unlock(&common::wrapper_mutex); // we get the mutex back, time to unlock
    pthread_mutex_unlock(&common::wrapper_submitter_mutex); // let other threads request for files

    if (number == 0) {
        pthread_join(common::wrapper_thread, nullptr); // finished
    }
    return result;
}


int128_t mul_inv(int128_t a, int128_t b) {
    int128_t b0 = b, t, q;
    int128_t x0 = 0, x1 = 1;
    if (b == 1) {
        return 1;
    }
    while (a > 1) {
        q = a / b;
        t = b, b = a % b, a = t;
        t = x0, x0 = x1 - q * x0, x1 = t;
    }
    if (x1 < 0) x1 += b0;
    return x1;
}

unsigned long long mod(string &s, bool positive_number_in_range) {
    uint128_t num, denum;
    int128_t temp128;
    long long int temp;
    unsigned long long int utemp;
    if (positive_number_in_range) {
        sscanf(s.c_str(), "%llu", &utemp);
        num = utemp;
    } else {
        sscanf(s.c_str(), "%lld", &temp); // there should be no overflow here, just reading
        temp128 = temp;
        temp128 = temp128 % ((int128_t) common::prime);
        if (temp128 < 0) {
            temp128 += ((int128_t) common::prime);
        }
        num = temp128;
    }

    unsigned long pos = s.find('/');
    if (pos != string::npos) {
        if (positive_number_in_range) {
            sscanf(s.c_str() + pos + 1, "%llu", &utemp);
            denum = utemp;
        } else {
            sscanf(s.c_str() + pos + 1, "%lld", &temp);
            if (temp < 0) {
                cout << "Bad denominator: " << s << endl;
                throw 1;
                // this cannot happen. we are reading a small number and the denominator cannot be big or negative
            }
            temp128 = temp;
            temp128 = temp128 % ((int128_t) common::prime);
            denum = temp128;
        }

        denum = mul_inv(denum, common::prime);
        num *= denum;
        num %= common::prime;
    }
    return num;
}

string ReplaceAll(string str, const string &from, const string &to) {
    size_t start_pos = 0;
    while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
    }
    return str;
}

string ReplaceAllVariables(string str) {
    for (const auto &variable_replacement : common::variable_replacements) {
        str = ReplaceAll(str, variable_replacement.first, "(" + variable_replacement.second + ")");
    }
    return str;
}

void getUserName(char *buf) {
    uid_t uid = geteuid();
    struct passwd *pw = getpwuid(uid);
    if (pw) {
        strcpy(buf, pw->pw_name);
    } else {
        strcpy(buf, "NONAME");
    }
}
